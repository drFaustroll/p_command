CMAKE_POLICY(SET CMP0048 NEW)
project(command
  LANGUAGES Fortran
  VERSION 1.0.0)

cmake_minimum_required(VERSION 3.02)

add_executable(oop.x src/oop-potentials.F90)
add_executable(naive.x src/naive-potentials.F90)
