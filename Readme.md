Problem
-------


Let us assume we have a system of N particles of m types interacting via pair potentials.

To exemplify let us assume we have two types of particles or what is called a binary mixture A, B.
This will give us three interaction potentials, between AA, BB and AB. we assume the interaction is a parametrized function of
distance between the particles. Depending on the pair the functional form may differ

example of potentials

```math
   U(r_{ij}) =\left\{
\begin{array}{lr}
4ε_{AB}\left[ \left(\frac{σ_{AB}}{r_{ij}}\right)^{12}-\left(\frac{σ_{AB}}{r_{ij}}\right)^6\right] & r \leq r_c\\
0 & r >  r_c \\
\end{array}
\right.
```
known as Lennard-Jones,$`r_c`$ is called potential cutt off. in practice a list of all atoms within the cutoff is precomputed and only
the non zero branch is evaluated.

or another
https://doi.org/10.1039/C9CP05445F

```math
  U(r_{ij}) = \left\{
\begin{array}{lr}
ε_{AB}α_{AB}\left[ \left(\frac{σ_{AB}}{r_{ij}}\right)^2 -1\right]\left[ \left(\frac{r_{AB}^c}{r_{ij}}\right)^2 -1\right]^2 & r \leq r_{AB}^c \\
0 & r >  r_{AB}^c \\
\end{array}
\right.

```
which
```math
α_{AB} = \frac{1}{4}\left(\frac{r_{AB}^c}{σ_{AB}}\right)^2\left( \frac{3}{\left(\frac{r_{AB}^c}{σ_{AB}}\right)^2-1}\right)^3
```
for this potential since the cutoff is per pair the cut off for precomputed list $`r_c = max(r_{AA}^c,r_{AB}^c,r_{BB}^c)`$

Solution
--------

includes the [naive implementation](src/naive-potentials.F90)

Pattern
-------

includes the [pattern implementation](src/oop-potentials.F90)

Perfomance
----------

any perfomance considerents

